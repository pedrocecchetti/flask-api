import random
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    mynum = int(random.random() * 100)
    return f"""
    <p>Hello, World {mynum} !</p>
    <form>
    <button type='submit'> CLICK ME</button></form>"""
